angular.module("MyApp")
//--- Bloque directivas:

//----Directiva facturas		
	.directive('cuerpoFactura',cuerpoFactura)

//--Para el preloader	
	.directive('loading',   ['$http' ,function ($http)
    {
        return {
            restrict: 'A',
            link: function (scope, elm, attrs)
            {
                scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };

                scope.$watch(scope.isLoading, function (v)
                {
                    if(v){
                        elm.show();
                    }else{
                        elm.hide();
                    }
                });
            }
        };

    }])
//--
function cuerpoFactura($compile){
	//--
	return {

				restrict: 'E',
				require: '^ngChange',
				transclude: true,
				scope:{
					numero:'@'
				},
				templateUrl: "templates/cuerpo_factura.html",
				controller:"facturaController",
				replace:true
	}
	//--
}
