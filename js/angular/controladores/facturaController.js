angular.module("MyApp")
	.controller("facturaController", function($scope,$http,$location,$compile){
		//Cuerpo declaraciones
		$scope.factura = {
							"id":"",
							"fecha":"",
							"folio":"",
							"nombre_comprador":"",
							"detalle":{
											"unidad":[],
											"precio":[],
											"cantidad":[],
											"subtotal":[]
							},
							"total":""

		}
		$scope.factura_registro = []
		$scope.id_seleccionado_qs = 0
		$scope.tabla = 0;							
		$scope.numero = 0
		$("#fecha_factura").val("01/01/2017");
		$scope.agregar_producto = function(){
			numero = $scope.numero
			$scope.numero = $scope.numero + 1 
			
			var template = '<div class="row fila_factura" id="fila'+$scope.numero+'">\
									<div class="col-lg-3" class="detalle_fact">\
										<input id="unidad'+numero+'" name="unidad'+numero+'" type="text" class="form-control unidad" placeholder="unidad" ng-model="factura.detalle.unidad['+numero+']">\
									</div>\
									<div class="col-lg-3" class="detalle_fact">\
										<input id="precio'+numero+'" name="precio'+numero+'" type="text" class="form-control precio" placeholder="Precio" dat="'+numero+'" ng-change="calcular_subtotal('+numero+')" ng-model="factura.detalle.precio['+numero+']">\
									</div>\
									<div class="col-lg-3" class="detalle_fact">\
										<input id="cantidad'+numero+'" name="cantidad'+numero+'" type="text" class="form-control cantidad" placeholder="cantidad" dat="'+numero+'" ng-change="calcular_subtotal('+numero+')" ng-model="factura.detalle.cantidad['+numero+']" >\
									</div>\
									<div class="col-lg-3" class="detalle_fact">\
										<input id="total'+numero+'"  type="text" class="form-control subtotal" placeholder="Sub-total" ng-model="factura.detalle.subtotal['+numero+']" readonly>\
									</div>\
								</div>'							
		
			$("#cuerpo_detalle_factura").append($compile(template)($scope)).fadeIn("slow");  

		}
		$scope.quitar_producto = function(){
			console.log($scope.numero)
			if($scope.numero=="1"){
				showErrorMessage("No puede quitar la primera fila");
			}else{
				$("#fila"+$scope.numero).remove()
				$scope.numero = $scope.numero -1
				$scope.factura.detalle.cantidad.splice($scope.numero, 1);
				$scope.factura.detalle.unidad.splice($scope.numero, 1);
				$scope.factura.detalle.precio.splice($scope.numero, 1);
				$scope.factura.detalle.subtotal.splice($scope.numero, 1);		
				//--Actualizo el total global
				$scope.factura.total = 0
				$.each($scope.factura.detalle.subtotal, function( index, value ) {
			  		$scope.factura.total = parseFloat($scope.factura.total) + parseFloat(value) 
				});
			}			
		}

		$scope.calcular_subtotal = function(event){
			var index = event		
			if(($("#precio"+index).val()!="")&&($("#cantidad"+index).val()!="")){
				var sub_total = parseInt($("#cantidad"+index).val())*parseFloat($("#precio"+index).val())
				//$("#total"+index).val(sub_total)
				$scope.factura.detalle.subtotal[index] = sub_total
				//Sumo al total global
				
			}else{
				$("#total"+index).val("")
			}
			$scope.factura.total = 0;
			/*$(".subtotal").each(function(index,value){
				caja = $(this).attr("id")
				alert(caja)
				if($("#"+caja).val()!=""){
					valor_caja = $("#"+caja).val()
					$scope.factura.total = parseFloat($scope.factura.total) + parseFloat(valor_caja) 
				}
			});*/	
			//----
			$.each($scope.factura.detalle.subtotal, function( index, value ) {
			  	$scope.factura.total = parseFloat($scope.factura.total) + parseFloat(value) 
			});
			//----

		}
		//Para guardar
		$scope.registrar_factura = function(){
			console.log("iD:"+$scope.factura.id)
			//--Para guardar
			if($scope.factura.id==""){
				console.log("Guardar");
				/*$(".subtotal").each(function(index,value){
					indice = index + 1
					$scope.factura.detalle.unidad.push($("#unidad"+indice).val())
					$scope.factura.detalle.precio.push($("#precio"+indice).val())
					$scope.factura.detalle.cantidad.push($("#cantidad"+indice).val())
					$scope.factura.detalle.subtotal.push($("#total"+indice).val())
					
				});*/	
				$scope.factura.id = $scope.factura_registro.length + 1

				$scope.factura_registro.push($scope.factura)
				showSuccess("El registro fue realizado de manera exitosa");
				$scope.volver()
				console.log($scope.factura_registro)
	
			}else//para modificar
			{
				//Limpio el detalle	
				console.log("Modificar");
				/*$scope.factura.detalle.unidad=[]
				$scope.factura.detalle.precio=[]
				$scope.factura.detalle.cantidad=[]
				$scope.factura.detalle.subtotal=[]*/
				//vuelvo a hacer el recorrido
				/*$(".subtotal").each(function(index,value){
					indice = index + 1
					$scope.factura.detalle.unidad.push($("#unidad"+indice).val())
					$scope.factura.detalle.precio.push($("#precio"+indice).val())
					$scope.factura.detalle.cantidad.push($("#cantidad"+indice).val())
					$scope.factura.detalle.subtotal.push($("#total"+indice).val())
				});*/	
				//--reasigno el vector en la posicion
				
				//$scope.factura_registro[$scope.factura.id] = $scope.factura
				showSuccess("El registro fue actualizado de manera exitosa");
				$scope.tabla = 1
				console.log($scope.factura_registro)
			}
			//--Fin de modificar...
		}
		//--Para actualizar
		$scope.ver_factura = function(event){
			$scope.volver();
			$scope.detalle = {
									"precio":"",
									"unidad":"",
									"cantidad":"",
									"total":""
			}
			$scope.tabla = 0
			var caja = event.currentTarget.id;
			id = $("#"+caja).attr("data");
			id = parseInt(id)-1

			console.log("Id seleccionado:"+id)
			$scope.factura = $scope.factura_registro[id]
			//--
			/*$scope.detalle.precio = $scope.factura.detalle.precio
			$scope.detalle.unidad = $scope.factura.detalle.unidad
			$scope.detalle.cantidad = $scope.factura.detalle.cantidad*/
			//--
			contador = 0
			$.each($scope.factura.detalle.unidad , function( index, value ) {
			  	$scope.numero = 1
			  	contador = contador + 1
			  	if(contador>1)
					$scope.agregar_producto()
				$("#unidad"+contador).val(value)
			});
			contador = 0
			$.each($scope.factura.detalle.cantidad , function( index, value ) {
				console.log("cantidad:"+value);
			  	contador = contador + 1
				$("#cantidad"+contador).val(value)
			});
			contador = 0
			$.each($scope.factura.detalle.precio , function( index, value ) {
				console.log("precio:"+value);
			  	contador = contador + 1
				$("#precio"+contador).val(value)
				//--Calculo el subtotal
				if(($("#precio"+contador).val()!="")&&($("#cantidad"+contador).val()!="")){
					var sub_total = parseInt($("#cantidad"+contador).val())*parseFloat($("#precio"+contador).val())
					$("#total"+contador).val(sub_total)
					//Sumo al total global
					
				}else{
					$("#total"+index).val("")
				}
				//--
			});

			//---
			/*precio = 0
			$.each($scope.factura.detalle.precio , function( index, value ) {
			  	contador = contador + 1
				$("#precio"+contador).val(value)
			});*/
			  	/*$scope.agregar_producto();
			  	$("#precio"+contador)
			  	$scope.calcular_subtotal();*/
			console.log($scope.factura)
		}
		//--Para eliminar
		$scope.eliminar_registro = function(event){
			var caja = event.currentTarget.id;
			$scope.id_seleccionado_qs = $("#"+caja).attr("data");
			$scope.id_seleccionado_qs = $scope.id_seleccionado_qs - 1;
			console.log("Id seleccionado:"+$scope.id_seleccionado_qs)
			$("#cabecera_mensaje").text("Información:");
			if ($scope.estatus_seleccionado_qs==0){
				mensaje = "Desea modificar el estatus de este registro a publicado? ";
			}else{
				mensaje = "Desea modificar el estatus de este registro a inactivo? ";
			}
			$("#cuerpo_mensaje").text(mensaje);
			$("#modal_mensaje").modal("show");
		}
		//--Si acepta eliminar el registro
		$scope.procesar_operacion = function(){
			$scope.factura_registro.splice($scope.id_seleccionado_qs, 1);
			console.log($scope.factura_registro)
		}
		$scope.consultar_factura = function(){
			$scope.tabla = 1;
		}
		$scope.volver = function(){
			$scope.tabla = 0;
			$scope.factura = {
							"id":"",
							"fecha":"",
							"folio":"",
							"nombre_comprador":"",
							"detalle":{
											"unidad":[],
											"precio":[],
											"cantidad":[],
											"subtotal":[]
							},
							"total":""

			}

			for($i=$scope.numero;$i!=1;$i--){
				$scope.quitar_producto()
			}
			$("#unidad1,#precio1,#cantidad1,#total1").val("")
		}
		//Imprimir pdf
		$scope.ver_folleto = function(){
			showErrorMessage("Lo sentimos por los momentos esta opción no esta disponible!")
		}
		//--Bloque de llamados
		$scope.agregar_producto()
	});