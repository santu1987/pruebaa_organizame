angular.module("MyApp")
	.controller("MainController", function($scope,$http,$location,sesionFactory){
		$scope.inicio = {};
		$scope.login = ""
		$scope.clave = ""
		//---------------------------------------
		$scope.inciar_sesion = function(){
			//alert($scope.login+","+$scope.clave);
			valor_inicio =	sesionFactory.sesion_usuario($scope.login,$scope.clave);
		//	alert(valor_inicio);
			(valor_inicio=="1")? $location.path("/panelUs"): showErrorMessage("Ocurrió un error inesperado");
		}
		//
	
		//--
		$scope.validar_inicio_sesion = function(){
			if($scope.login==""){
				showErrorMessage("Debe ingresar nombre de usuario");
				$("#login_p").focus();
				return false;
			}else
			if($scope.clave==""){
				showErrorMessage("Debe ingresar clave de usuario");
				$("#clave").focus();
				return false;
			}else{
				return true;
			}
		}

		$scope.cerrar_sesion = function(){
			sesionFactory.cerrar_sesion(function(data){
				if(data["respuesta"]=="cerrado"){
					$location.path("/");
				}
			});
		}
		//---------------------------------------
		
		$("#login_p,#clave").keypress(function(e) {
		  if(e.which==13){
		    	$scope.inciar_sesion();
		  }
		}); 
		//---------------------------------------
	});