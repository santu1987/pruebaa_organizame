angular.module("MyApp",["ngRoute","ngSanitize"])
.config(function($routeProvider,$locationProvider){
	$routeProvider
		.when("/",{
			controller: "MainController",
			templateUrl: "templates/inicio_sesion.html"
		})
		.when("/panelUs",{
			controller:"facturaController",
			templateUrl : "templates/factura.html"
		})
		.otherwise({
	    	redirectTo: '/'
		});
		//$locationProvider.html5Mode(true);
});